# MSite

Projekt polegał na przekształceniu wysłanej przez zleceniodawcę strony stworzonej za pomocą HTML (w bardzo starym stylu, pozycjonowanie elementów następowało za pomocą tabel) w stronę o innej treści. W ramach rozmów przyjęta została również opcja zmiany wyglądu i struktury strony. Wszystkie elementy graficzne (łącznie z tłem) wskazane zostały przez zleceniodawcę.

Zleceniodawca: Monika Jasińska